import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Clinic } from '../../../../../common/clinic'
import { Employe } from '../../../../../common/employe';
import { Pet } from '../../../../../common/pet';
import { Owner } from '../../../../../common/owner';
import { Bill } from '../../../../../common/bill';


@Injectable({
  providedIn: 'root'
})
export class CommunicationService {

  url = "http://localhost:3000/"
  //url ="https://www.xn--vto-sans-frontires-5vbf.com/"


  constructor(private http: HttpClient) { }
  /***************************************************CLINICs******************************************************/
  public getClinics(): Observable<any> {
    console.log('send request GET');
    return this.http.get(this.url + "clinic/clinics");
  }

  public deleteClinic(clinicNb: string): Observable<any> {
    console.log('send request DELETE');
    return this.http.delete<string>(this.url + "clinic/" + clinicNb);
  }

  public postClinic(clinic: Clinic): Observable<any> {
    console.log('send request POST');
    console.log(clinic);
    return this.http.post(this.url + "clinic/", clinic, { responseType: 'text' });
  }
  /***************************************************CLINICs******************************************************/


  /***************************************************EMPLOYES******************************************************/
  public getEmploye(): Observable<any> {
    console.log('send request GET');
    return this.http.get(this.url + "employe/employes");
  }

  public getEmployeAgeFilter(): Observable<any> {
    console.log('send request GET');
    return this.http.get(this.url + "employe/employes40");
  }

  public deleteEmploye(employeId: string): Observable<any> {
    console.log('send request DELETE');
    return this.http.delete<string>(this.url + "employe/" + employeId);
  }

  public postEmploye(employe: Employe): Observable<any> {
    console.log('send request POST');
    console.log(employe);
    return this.http.post(this.url + "employe/", employe, { responseType: 'text' });
  }
  /***************************************************EMPLOYES******************************************************/

  /***************************************************PETS******************************************************/
  public getPets(): Observable<any> {
    console.log('send request GET');
    return this.http.get(this.url + "pet/pets");
  }

  public getPetsWithFilter(name: string): Observable<any> {
    console.log('send request GET');
    return this.http.get(this.url + "pet/" + name);
  }

  public getPetsByOwnerId(id: string): Observable<any> {
    console.log(id);
    console.log('send request GET');
    return this.http.get<string>(this.url + "pet/" + id);
  }

  public deletePet(petId: string): Observable<any> {
    console.log('send request DELETE');
    return this.http.delete<string>(this.url + "pet/" + petId);
  }

  public postPet(pet: Pet): Observable<any> {
    console.log('send request POST');
    console.log(pet);
    return this.http.post(this.url + "pet/", pet, { responseType: 'text' });
  }
  /***************************************************PETS******************************************************/

  /***************************************************Owner******************************************************/
  public getOwners(): Observable<any> {
    console.log('send request GET');
    return this.http.get(this.url + "owner/owners");
  }

  public getOwnersbyIdClinics(clinicId: string): Observable<any> {
    console.log('send request GET');
    return this.http.get<string>(this.url + "owner/" + clinicId);
  }


  public deleteOwner(ownerId: string): Observable<any> {
    console.log('send request DELETE');
    return this.http.delete<string>(this.url + "owner/" + ownerId);
  }

  public postOwner(owner: Owner): Observable<any> {
    console.log('send request POST');
    console.log(owner);
    return this.http.post(this.url + "owner/", owner, { responseType: 'text' });
  }
  /***************************************************Owner******************************************************/


  /***************************************************Salary******************************************************/
  public getClinicsSalarys(): Observable<any> {
    console.log('send request GET');
    return this.http.get(this.url + "salary/clinicsSalarys");
  }
  /***************************************************Salary******************************************************/

  /***************************************************Occurences******************************************************/
  public getPetOccurences(): Observable<any> {
    console.log('send request GET');
    return this.http.get(this.url + "occurence/occurences");
  }
  /***************************************************Occurences******************************************************/

  /***************************************************Occurences******************************************************/
  public getTraitements(): Observable<any> {
    console.log('send request GET');
    return this.http.get(this.url + "traitement/traitements");
  }

  public getTraitementsByPetId(Id: string): Observable<any> {
    console.log('send request GET');
    return this.http.get(this.url + "traitement/" + Id);
  }
  /***************************************************Occurences******************************************************/

  /***************************************************BILLS******************************************************/
  public postBill(bill: Bill): Observable<any> {
    console.log('send request POST');
    console.log(bill);
    return this.http.post(this.url + "bill/", bill, { responseType: 'text' });
  }

  public getBill(billId: string): Observable<any> {
    console.log('send request GET');
    return this.http.get<string>(this.url + "bill/" + billId);
  }

  /***************************************************BILLS******************************************************/

  /***************************************************DETAILS TREATMENTS******************************************************/
  public getTreatementDetails(treatmentId: string): Observable<any> {
    console.log('send request GET');
    return this.http.get<string>(this.url + "detail/" + treatmentId);
  }
  /***************************************************DETAILS TREATMENTS******************************************************/


}

