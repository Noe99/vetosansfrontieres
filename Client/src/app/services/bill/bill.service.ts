import { Injectable } from '@angular/core';
import { Bill } from '../../../../../common/bill';

@Injectable({
  providedIn: 'root'
})
export class BillService {
  public currentBill: Bill;
  constructor() { }
}
