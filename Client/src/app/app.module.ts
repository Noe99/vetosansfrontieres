import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app/app.component';
import { AcceuilComponent } from './components/pages/Acceuil/acceuil/acceuil.component';
import { MenuComponent } from './components/menu/menu/menu.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { CliniqueComponent } from './components/pages/Clinique/clinique/clinique.component';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { EmployeComponent } from './components/pages/employe/employe.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { AnimauxComponent } from './components/pages/animaux/animaux.component';
import { ProprietaireComponent } from './components/pages/proprietaire/proprietaire.component';
import { SalaireComponent } from './components/pages/salaire/salaire.component';
import { OccurenceComponent } from './components/pages/animaux/occurence/occurence.component';
import { DetailComponent } from './components/pages/proprietaire/detail/detail.component'
import {MatSelectModule} from '@angular/material/select';
import { TraitementComponent } from './components/pages/traitement/traitement.component';
import { BillComponent } from './components/pages/bill/bill.component';
@NgModule({
  declarations: [
    AppComponent,
    AcceuilComponent,
    MenuComponent,
    CliniqueComponent,
    EmployeComponent,
    AnimauxComponent,
    ProprietaireComponent,
    SalaireComponent,
    OccurenceComponent,
    DetailComponent,
    TraitementComponent,
    BillComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatTableModule,
    MatExpansionModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
