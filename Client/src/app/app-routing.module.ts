import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AcceuilComponent } from './components/pages/Acceuil/acceuil/acceuil.component';
import { CliniqueComponent } from './components/pages/Clinique/clinique/clinique.component';
import { EmployeComponent } from './components/pages/employe/employe.component';
import { AnimauxComponent } from './components/pages/animaux/animaux.component'
import { ProprietaireComponent } from './components/pages/proprietaire/proprietaire.component'
import { SalaireComponent } from './components/pages/salaire/salaire.component'
import { OccurenceComponent } from './components/pages/animaux/occurence/occurence.component'
import { DetailComponent } from './components/pages/proprietaire/detail/detail.component'
import { TraitementComponent } from './components/pages/traitement/traitement.component';
import { BillComponent } from './components/pages/bill/bill.component';

const routes: Routes = [
  { path: '', redirectTo: '/acceuil', pathMatch: 'full' },
  { path: 'acceuil', component: AcceuilComponent },
  { path: 'clinique', component: CliniqueComponent },
  { path: 'employe', component: EmployeComponent },
  { path: 'animaux', component: AnimauxComponent },
  { path: 'occurence', component: OccurenceComponent },
  { path: 'proprietaire', component: ProprietaireComponent },
  { path: 'salaire', component: SalaireComponent },
  { path: 'detail', component: DetailComponent },
  { path: 'traitement', component: TraitementComponent },
  { path: 'facture/:id', component: BillComponent },
  { path: '**', redirectTo: '/acceuil' },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
