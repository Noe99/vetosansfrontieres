import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

import { Employe } from '../../../../../../common/employe';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, throwError } from 'rxjs';
import { SelectionModel } from '@angular/cdk/collections';
import { CommunicationService } from 'src/app/services/com/communication.service';
@Component({
  selector: 'app-employe',
  templateUrl: './employe.component.html',
  styleUrls: ['./employe.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class EmployeComponent implements OnInit {
  ageFilter40: boolean = false;
  EmployeList: Employe[] = [{
    id: 'empty',
    name: 'empty',
    phonenb: 'empty',
    birthdate: 'empty',
    sexe: 'empty',
    nas: 'empty',
    fonction: 'empty',
    salary: 0,
  }];
  displayedColumns: string[] = ['select', 'id', 'name', 'phonenb', 'birthdate', 'sexe', 'nas', 'fonction', 'salary'];
  dataSource = new MatTableDataSource<Employe>(this.EmployeList);
  selection = new SelectionModel<Employe>(true, []);



  constructor(private communicationService: CommunicationService) {

  }

  ngOnInit(): void {
    this.getEmployes();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Employe): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id}`;
  }

  /**
   * getEmployes
   */
  public getEmployes(): void {
    console.log('start request GET');
    if (this.ageFilter40) {
      this.communicationService.getEmployeAgeFilter().subscribe((data: Employe[]) => {
        this.EmployeList = data;
        for (let e of this.EmployeList) {
          e.birthdate = this.changeDate(e.birthdate);
        }
        this.dataSource = new MatTableDataSource<Employe>(this.EmployeList);
      });
    } else {
      this.communicationService.getEmploye().subscribe((data: Employe[]) => {
        this.EmployeList = data;
        for (let e of this.EmployeList) {
          e.birthdate = this.changeDate(e.birthdate);
        }
        this.dataSource = new MatTableDataSource<Employe>(this.EmployeList);
      });
    }
  }

  public updateEmployesList(): void {
    this.getEmployes();
  }
  public deleteEmployes(): void {
    console.log('start request DELETE');
    let selectedEmployes: Employe[] = this.selection.selected;
    for (let i = 0; i < selectedEmployes.length; i++) {
      let employe: Employe = selectedEmployes[i];
      console.log('delete clinic number : ' + employe.id);
      this.communicationService.deleteEmploye(employe.id).subscribe((data: Employe[]) => {
        console.log('DELETE in progress ...');
        this.updateEmployesList();
      });
    }
  }

  public addEmploye(id: string,
    name: string,
    phonenb: string,
    birthdate: string,
    sexe: string,
    nas: string,
    fonction: string,
    salary: number): void {

    let employe: Employe = {
      id: id,
      name: name,
      phonenb: phonenb,
      birthdate: this.changeDateFormat(birthdate),
      sexe: sexe,
      nas: nas,
      fonction: fonction,
      salary: salary
    }

    console.log(employe.nas);
    this.communicationService.postEmploye(employe).subscribe((data: Employe[]) => {
      console.log('POST in progress ...');
      this.updateEmployesList();
    });
  }


  public changeDateFormat(date: string): string {
    return date[6] + date[7] + date[8] + date[9] + '-' + date[3] + date[4] + '-' + date[0] + date[1];;
  }

  public changeDate(date: string): string {
    return date[0] + date[1] + date[2] + date[3] + '-' + date[5] + date[6] + '-' + date[8] + date[9];;
  }

  public ageFilter(event: boolean): void {
    this.ageFilter40 = event;
    this.updateEmployesList();
  }
}
