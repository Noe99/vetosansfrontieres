import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, throwError } from 'rxjs';
import { Owner } from '../../../../../../common/owner';
import { SelectionModel } from '@angular/cdk/collections';
import { Clinic } from '../../../../../../common/clinic';
import { Pet } from '../../../../../../common/pet';
import { Bill } from '../../../../../../common/bill';
import { Traitement } from '../../../../../../common/traitement';
import { CommunicationService } from 'src/app/services/com/communication.service';
import { BillService } from 'src/app/services/bill/bill.service';
import { formatDate } from '@angular/common';


@Component({
  selector: 'app-traitement',
  templateUrl: './traitement.component.html',
  styleUrls: ['./traitement.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class TraitementComponent implements OnInit {
  autorisationToGenrateBill: boolean = false;
  billID: string = '/traitement';
  petID: string = '';
  expandedElement: Traitement | null;
  PetList: Pet[] = [];
  listClinicsId: string[] = [];
  traitements: Traitement[] = [{
    examid: 'empty',
    date: 'empty',
    petid: 'empty',
    veterinaryid: 'empty',
    traitementid: 'empty',
    billid: 'empty',
    quantity: 0,
    startdate: 'empty',
    enddate: 'empty',
  }];

  columnsToDisplay = ['examid', 'date', 'petid', 'veterinaryid', 'traitementid', 'billid', 'quantity', 'startdate', 'enddate'];

  constructor(private communicationService: CommunicationService) {
  }

  ngOnInit(): void {
    this.getTraitements();
    this.getClinicsId();
    this.getPets();
  }

  public getClinicsId(): void {
    console.log('start request GET');
    this.communicationService.getClinics().subscribe((data: Clinic[]) => {
      for (let c of data) {
        this.listClinicsId.push(c.id);
      }
      console.log(this.listClinicsId);
    });
  }

  public getPets(): void {
    console.log('start request GET');
    this.communicationService.getPets().subscribe((data: Pet[]) => {
      this.PetList = data;
      for (let e of this.PetList) {
        e.birthdate = this.changeDate(e.birthdate);
        e.inscriptiondate = this.changeDate(e.inscriptiondate);
      }
    });
  }

  public getTraitements(): void {
    this.communicationService.getTraitements().subscribe((data: Traitement[]) => {
      this.traitements = data;
      for (let t of this.traitements) {
        t.date = this.changeDate(t.date);
        t.startdate = t.startdate === null ? 'null' : this.changeDate(t.startdate);
        t.enddate = t.enddate === null ? 'null' : this.changeDate(t.enddate);
      }
      console.log(this.traitements);
    });
  }

  public getTraitementById(id: string): void {
    this.petID = id;
    this.communicationService.getTraitementsByPetId(id).subscribe((data: Traitement[]) => {
      this.traitements = data;
      console.log(this.traitements);
      if (this.traitements.length > 0) {
        this.billID = '/facture/' + this.traitements[0].billid;
        this.autorisationToGenrateBill = true;
      } else {
        this.autorisationToGenrateBill = false;
      }
    });
  }

  public updateTraitementList(id: string): void {
    this.getTraitementById(id);
  }


  public generateBill(): void {
    let bill: Bill;
    if (this.traitements.length > 0) {
      if (this.traitements[0].billid === '') {
        bill = {
          id: 'B' + Math.floor(Math.random() * (10000 - 1 + 1)) + 1,
          ownerid: this.PetList[0].ownerid,
          petid: this.petID,
          veterinaryid: this.traitements[0].veterinaryid,
          date: this.changeDate(formatDate(new Date(), 'yyyy/MM/dd', 'en')),
          paymentmethod: '',
          paymentaccepted: '',
          total: 0,
        }
        this.addBill(bill);
        this.billID = '/facture/' + bill.id;
        console.log(this.billID);
      } else {
        this.billID = '/facture/' + this.traitements[0].billid;
      }
    }
  }

  public addBill(bill: Bill): void {
    this.communicationService.postBill(bill);
  }


  public changeDateFormat(date: string): string {
    return date[6] + date[7] + date[8] + date[9] + '-' + date[3] + date[4] + '-' + date[0] + date[1];;
  }

  public changeDate(date: string): string {
    return date[0] + date[1] + date[2] + date[3] + '-' + date[5] + date[6] + '-' + date[8] + date[9];;
  }


}
