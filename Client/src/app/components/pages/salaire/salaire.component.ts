import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ClinicSalary } from '../../../../../../common/clinicSalary';
import { SelectionModel } from '@angular/cdk/collections';
import { CommunicationService } from 'src/app/services/com/communication.service';

@Component({
  selector: 'app-salaire',
  templateUrl: './salaire.component.html',
  styleUrls: ['./salaire.component.scss']
})
export class SalaireComponent implements OnInit {
  ClinicSalaryList: ClinicSalary[] = [{
    id: 'empty',
    salary: 0,
  }];
  displayedColumns: string[] = ['select', 'id', 'salary'];
  dataSource = new MatTableDataSource<ClinicSalary>(this.ClinicSalaryList);
  selection = new SelectionModel<ClinicSalary>(true, []);



  constructor(private communicationService: CommunicationService) {

  }

  ngOnInit(): void {
    this.getClinicSalarys();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: ClinicSalary): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id}`;
  }

  /**
   * getClinicSalarys
   */
  public getClinicSalarys(): void {
    console.log('start request GET');
    this.communicationService.getClinicsSalarys().subscribe((data: ClinicSalary[]) => {
      this.ClinicSalaryList = data;
      for (let c of this.ClinicSalaryList) {
        if (c.salary === null) {
          c.salary = 0;
        }
      }
      this.dataSource = new MatTableDataSource<ClinicSalary>(this.ClinicSalaryList);
    });
  }

  public updateClinicSalarysList(): void {
    this.getClinicSalarys();
  }

}