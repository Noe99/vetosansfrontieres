import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BillService } from 'src/app/services/bill/bill.service';
import { CommunicationService } from 'src/app/services/com/communication.service';
import { Bill } from '../../../../../../common/bill';
import { Traitement } from '../../../../../../common/traitement';
import { Details } from '../../../../../../common/details';

export interface Medic {
  id: string;
  quantity: number;
  price: number;
}

@Component({
  selector: 'app-bill',
  templateUrl: './bill.component.html',
  styleUrls: ['./bill.component.scss']
})

export class BillComponent implements OnInit {
  public medicins: Medic[] = [];
  public bill: Bill;
  public traitements: Traitement[] = [];
  public traitementsDetails: Details[] = [];
  constructor(private router: ActivatedRoute, private communicationService: CommunicationService) {
  }

  ngOnInit(): void {
    this.communicationService.getBill(this.router.snapshot.params.id).subscribe(data => {
      this.bill = data[0];
      this.bill.date = this.changeDate(this.bill.date);
      console.log(this.bill);
      this.getTraitementById(this.bill.petid);
      console.log(this.traitementsDetails);
    })

    if (this.bill === undefined) {
      this.bill = {
        id: '',
        ownerid: '',
        petid: '',
        veterinaryid: '',
        date: '',
        paymentmethod: '',
        paymentaccepted: '',
        total: 0,
      };
    }

  }



  public changeDate(date: string): string {
    return date[0] + date[1] + date[2] + date[3] + '-' + date[5] + date[6] + '-' + date[8] + date[9];;
  }

  public calculeTotal(): string {
    return '';
  }

  public getTraitementById(id: string): void {
    this.communicationService.getTraitementsByPetId(id).subscribe((data: Traitement[]) => {
      this.traitements = data;
      console.log(this.traitements);
      for (let i = 0; i < this.traitements.length; i++) {
        this.communicationService.getTreatementDetails(this.traitements[i].traitementid).subscribe((data2: Details[]) => {
          this.traitementsDetails.push(data2[0]);
          let medic: Medic = {
            id: data2[0].id,
            quantity: 0,
            price: data2[0].price
          }
          this.medicins.push(medic);
          for (let m of this.medicins) {
            m.quantity = this.findQuantity(m.id);
          }
        });
      }
    });
  }

  public getTraitementDetails(id: string): void {
    this.communicationService.getTreatementDetails(id).subscribe((data: Details[]) => {
      this.traitementsDetails.push(data[0]);
    });
  }

  public findQuantity(id: string): number {
    for (let t of this.traitements) {
      if (t.traitementid === id) {
        return t.quantity;
      }
    }
    return 0;
  }
}

