import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, throwError } from 'rxjs';
import { Pet } from '../../../../../../common/pet';
import { SelectionModel } from '@angular/cdk/collections';
import { Clinic } from '../../../../../../common/clinic';
import { Owner } from '../../../../../../common/owner';
import { CommunicationService } from 'src/app/services/com/communication.service';
@Component({
  selector: 'app-animaux',
  templateUrl: './animaux.component.html',
  styleUrls: ['./animaux.component.scss']
})
export class AnimauxComponent implements OnInit {
  listClinicsId: string[] = [];
  OwnerList: Owner[] = [];
  PetList: Pet[] = [{
    id: 'empty',
    id_clinic: 'empty',
    name: 'empty',
    type: 'empty',
    specie: 'empty',
    size: 0,
    weight: 0,
    description: 'empty',
    birthdate: 'empty',
    inscriptiondate: 'empty',
    state: 'empty',
    ownerid: 'empty',
  }];
  displayedColumns: string[] = ['select', 'id', 'id_clinic', 'name', 'type', 'specie', 'size',
    'weight', 'description', 'birthdate', 'inscriptiondate', 'state', 'ownerid'];
  dataSource = new MatTableDataSource<Pet>(this.PetList);
  selection = new SelectionModel<Pet>(true, []);



  constructor(private communicationService: CommunicationService) {

  }

  ngOnInit(): void {
    this.getPets();
    this.getClinicsId();
    this.getOwners();
  }

  public getClinicsId(): void {
    console.log('start request GET');
    this.communicationService.getClinics().subscribe((data: Clinic[]) => {
      for (let c of data) {
        this.listClinicsId.push(c.id);
      }
    });
  }

  public getOwners(): void {
    this.communicationService.getOwners().subscribe((data: Owner[]) => {
      this.OwnerList = data;
      console.log(this.OwnerList);
    });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Pet): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id}`;
  }

  /**
   * getPets
   */
  public getPets(): void {
    console.log('start request GET');
    this.communicationService.getPets().subscribe((data: Pet[]) => {
      this.PetList = data;
      for (let e of this.PetList) {
        e.birthdate = this.changeDate(e.birthdate);
        e.inscriptiondate = this.changeDate(e.inscriptiondate);
      }
      this.dataSource = new MatTableDataSource<Pet>(this.PetList);
    });
  }

  public nameFilter(name: string) {
    this.communicationService.getPetsWithFilter(name).subscribe((data: Pet[]) => {
      this.PetList = data;
      for (let e of this.PetList) {
        e.birthdate = this.changeDate(e.birthdate);
        e.inscriptiondate = this.changeDate(e.inscriptiondate);
      }
      this.dataSource = new MatTableDataSource<Pet>(this.PetList);
    });
  }
  public updatePetsList(): void {
    this.getPets();
  }
  public deletePets(): void {
    console.log('start request DELETE');
    let selectedPets: Pet[] = this.selection.selected;
    for (let i = 0; i < selectedPets.length; i++) {
      let Pet: Pet = selectedPets[i];
      console.log('delete Pet number : ' + Pet.id);
      this.communicationService.deletePet(Pet.id).subscribe((data: Pet[]) => {
        console.log('DELETE in progress ...');
        this.updatePetsList();
      });
    }
  }

  public addPets(id: string,
    id_clinic: string,
    name: string,
    type: string,
    specie: string,
    size: number,
    weight: number,
    description: string,
    birthdate: string,
    inscriptiondate: string,
    state: string,
    ownerid: string): void {

    let pet: Pet = {
      id: id,
      id_clinic: id_clinic,
      name: name,
      type: type,
      specie: specie,
      size: size,
      weight: weight,
      description: description,
      birthdate: this.changeDateFormat(birthdate),
      inscriptiondate: this.changeDateFormat(inscriptiondate),
      state: state,
      ownerid: ownerid,
    }
    this.communicationService.postPet(pet).subscribe((data: Pet[]) => {
      console.log('POST in progress ...');
      this.updatePetsList();
    });
  }

  public changeDateFormat(date: string): string {
    return date[6] + date[7] + date[8] + date[9] + '-' + date[3] + date[4] + '-' + date[0] + date[1];;
  }

  public changeDate(date: string): string {
    return date[0] + date[1] + date[2] + date[3] + '-' + date[5] + date[6] + '-' + date[8] + date[9];;
  }


}