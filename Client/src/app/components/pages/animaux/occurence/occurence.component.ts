import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, throwError } from 'rxjs';
import { Occurence } from '../../../../../../../common/occurence';
import { SelectionModel } from '@angular/cdk/collections';
import { CommunicationService } from 'src/app/services/com/communication.service';
@Component({
  selector: 'app-occurence',
  templateUrl: './occurence.component.html',
  styleUrls: ['./occurence.component.scss']
})
export class OccurenceComponent implements OnInit {
  OccurenceList: Occurence[] = [{
    name: 'empty',
    occurence: 0,
  }];
  displayedColumns: string[] = ['select', 'name', 'occurence'];
  dataSource = new MatTableDataSource<Occurence>(this.OccurenceList);
  selection = new SelectionModel<Occurence>(true, []);



  constructor(private communicationService: CommunicationService) {

  }

  ngOnInit(): void {
    this.getPets();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Occurence): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.name}`;
  }

  /**
   * getPets
   */
  public getPets(): void {
    console.log('start request GET');
    this.communicationService.getPetOccurences().subscribe((data: Occurence[]) => {
      this.OccurenceList = data;
      this.dataSource = new MatTableDataSource<Occurence>(this.OccurenceList);
    });
  }

  public updatePetsList(): void {
    this.getPets();
  }
}