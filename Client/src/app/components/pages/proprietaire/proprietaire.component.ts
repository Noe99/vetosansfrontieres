import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

import { MatTableDataSource } from '@angular/material/table';
import { Observable, throwError } from 'rxjs';
import { Owner } from '../../../../../../common/owner';
import { SelectionModel } from '@angular/cdk/collections';
import { CommunicationService } from 'src/app/services/com/communication.service';
@Component({
  selector: 'app-proprietaire',
  templateUrl: './proprietaire.component.html',
  styleUrls: ['./proprietaire.component.scss']
})
export class ProprietaireComponent implements OnInit {
  OwnerList: Owner[] = [{
    id: 'empty',
    id_clinic: 'empty',
    name: 'empty',
    adress: 'empty',
    phonenb: 'empty',
  }];
  displayedColumns: string[] = ['select', 'id', 'id_clinic', 'name', 'adress', 'phonenb'];
  dataSource = new MatTableDataSource<Owner>(this.OwnerList);
  selection = new SelectionModel<Owner>(true, []);



  constructor(private communicationService: CommunicationService) {

  }

  ngOnInit(): void {
    this.getOwners();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Owner): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id}`;
  }

  /**
   * getOwners
   */
  public getOwners(): void {
    console.log('start request GET');
    this.communicationService.getOwners().subscribe((data: Owner[]) => {
      this.OwnerList = data;
      this.dataSource = new MatTableDataSource<Owner>(this.OwnerList);
    });
  }

  public updateOwnersList(): void {
    this.getOwners();
  }
  public deleteOwners(): void {
    console.log('start request DELETE');
    let selectedOwners: Owner[] = this.selection.selected;
    for (let i = 0; i < selectedOwners.length; i++) {
      let Owner: Owner = selectedOwners[i];
      console.log('delete Owner number : ' + Owner.id);
      this.communicationService.deleteOwner(Owner.id).subscribe((data: Owner[]) => {
        console.log('DELETE in progress ...');
        this.updateOwnersList();
      });
    }
  }

  public addOwners(id: string,
    id_clinic: string,
    name: string,
    adress: string,
    phonenb: string,
  ): void {

    let Owner: Owner = {
      id: id,
      id_clinic: id_clinic,
      name: name,
      adress: adress,
      phonenb: phonenb,
    }
    this.communicationService.postOwner(Owner).subscribe((data: Owner[]) => {
      console.log('POST in progress ...');
      this.updateOwnersList();
    });
  }

  public changeDateFormat(date: string): string {
    return date[6] + date[7] + date[8] + date[9] + '-' + date[3] + date[4] + '-' + date[0] + date[1];;
  }

  public changeDate(date: string): string {
    return date[0] + date[1] + date[2] + date[3] + '-' + date[5] + date[6] + '-' + date[8] + date[9];;
  }


}