import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, throwError } from 'rxjs';
import { Owner } from '../../../../../../../common/owner';
import { SelectionModel } from '@angular/cdk/collections';
import { Clinic } from '../../../../../../../common/clinic';
import { Pet } from '../../../../../../../common/pet';
import { CommunicationService } from 'src/app/services/com/communication.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class DetailComponent implements OnInit {
  pets: Pet[] = [];
  expandedElement: Owner | null;
  listClinicsId: string[] = [];
  OwnerList: Owner[] = [{
    id: 'empty',
    id_clinic: 'empty',
    name: 'empty',
    adress: 'empty',
    phonenb: 'empty',
  }];
  columnsToDisplay = ['id', 'id_clinic', 'name', 'adress', 'phonenb'];

  constructor(private communicationService: CommunicationService) {
  }

  ngOnInit(): void {
    this.getOwners();
    this.getClinicsId();
  }

  public getClinicsId(): void {
    console.log('start request GET');
    this.communicationService.getClinics().subscribe((data: Clinic[]) => {
      for (let c of data) {
        this.listClinicsId.push(c.id);
      }
      console.log(this.listClinicsId);
    });
  }

  public getOwners(): void {
    console.log('start request GET');
    this.communicationService.getOwners().subscribe((data: Owner[]) => {
      this.OwnerList = data;
    });
  }

  public getPetDetails(id: string): void {
    this.communicationService.getPetsByOwnerId(id).subscribe((data: Pet[]) => {
      this.pets = data;
      console.log(this.pets);
    });


  }

  public updateOwnersList(id: string): void {
    console.log(id);
    this.communicationService.getOwnersbyIdClinics(id).subscribe((data: Owner[]) => {
      this.OwnerList = data;
    });
  }

  public changeDateFormat(date: string): string {
    return date[6] + date[7] + date[8] + date[9] + '-' + date[3] + date[4] + '-' + date[0] + date[1];;
  }

  public changeDate(date: string): string {
    return date[0] + date[1] + date[2] + date[3] + '-' + date[5] + date[6] + '-' + date[8] + date[9];;
  }


}
