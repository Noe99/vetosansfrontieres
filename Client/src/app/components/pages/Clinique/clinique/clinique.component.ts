import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Clinic } from '../../../../../../../common/clinic';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, throwError } from 'rxjs';
import { SelectionModel } from '@angular/cdk/collections';
import { CommunicationService } from 'src/app/services/com/communication.service';
@Component({
  selector: 'app-clinique',
  templateUrl: './clinique.component.html',
  styleUrls: ['./clinique.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class CliniqueComponent implements OnInit {
  clinicList: Clinic[] = [{
    id: 'empty',
    name: 'empty',
    adress: 'empty',
    phonenb: 'empty',
    faxnb: 'empty',
    managerid: 'empty',
    numberofemploye: 0,
  }];
  displayedColumns: string[] = ['select', 'id', 'name', 'adress', 'phonenb', 'faxnb', 'managerid', 'numberofemploye'];
  dataSource = new MatTableDataSource<Clinic>(this.clinicList);
  selection = new SelectionModel<Clinic>(true, []);



  constructor(private communicationService: CommunicationService) {

  }

  ngOnInit(): void {
    this.getClinics();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Clinic): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id}`;
  }

  /**
   * getClinics
   */
  public getClinics(): void {
    console.log('start request GET');
    this.communicationService.getClinics().subscribe((data: Clinic[]) => {
      this.clinicList = data;
      this.dataSource = new MatTableDataSource<Clinic>(this.clinicList);
    });
  }

  public updateClinicsList(): void {
    this.getClinics();
  }
  public deleteClinics(): void {
    console.log('start request DELETE');
    let selectedClinics: Clinic[] = this.selection.selected;
    for (let i = 0; i < selectedClinics.length; i++) {
      let clinic: Clinic = selectedClinics[i];
      console.log('delete clinic number : ' + clinic.id);
      this.communicationService.deleteClinic(clinic.id).subscribe((data: Clinic[]) => {
        console.log('DELETE in progress ...');
        this.updateClinicsList();
      });
    }
  }

  public addClinics(id: string,
    name: string,
    adress: string,
    phonenb: string,
    faxnb: string,
    managerid: string,
    numberofemploye: number): void {

    let clinic: Clinic = {
      id: id,
      name: name,
      adress: adress,
      phonenb: phonenb,
      faxnb: faxnb,
      managerid: managerid,
      numberofemploye: numberofemploye,
    }
    console.log(clinic.numberofemploye + 1);
    this.communicationService.postClinic(clinic).subscribe((data: Clinic[]) => {
      console.log('POST in progress ...');
      this.updateClinicsList();
    });
  }
}
