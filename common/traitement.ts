export interface Traitement {
    examid: string;
    date: string;
    petid: string;
    veterinaryid: string;
    traitementid: string;
    billid: string;
    quantity: number;
    startdate: string;
    enddate: string;
}