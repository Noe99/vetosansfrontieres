export interface Bill {
    id: string;
    ownerid: string;
    petid: string;
    veterinaryid: string;
    date: string;
    paymentmethod: string;
    paymentaccepted: string;
    total: number;
}