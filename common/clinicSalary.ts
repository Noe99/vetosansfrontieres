export interface ClinicSalary {
    id: string;
    salary: number;
}